package dw;
import robocode.*;
//import java.awt.Color;

/**
 * DWStraight - a robot by (developerWorks)
 */
public class DWStraight extends Robot
{
	/**
	 * run: DWStraight's default behavior
	 */
	public void run() {
		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:
		//setColors(Color.red,Color.blue,Color.green);
		
		turnLeft(getHeading());
		while(true) {
			// Replace the next 4 lines with any behavior you would like
			ahead(1000);
			turnRight(90);
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		fire(1);
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		turnLeft(180);
	}
	
}
