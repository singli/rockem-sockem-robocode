Background
----------

Join thousands around the world on one of the fastest, and certainly the most fun, ways to learn Java programming.

![Learn Java programming the fun way](http://www.ibm.com/developerworks/library/j-robocode/fig2.gif)

Code and test your own robots. Fight against computerized opponents.

![Battle of the robots](http://www.ibm.com/developerworks/library/j-robocode/fig1.gif)

Please see [Rock'em, sock'em Robocode!](http://www.ibm.com/developerworks/library/j-robocode/)  for detailed information on working with this code.

The code in this repository may be periodically updated.  Download the [latest distribution zip file here](https://bitbucket.org/singli/rockem-sockem-robocode/downloads/coderobo1_20140116.zip).

### UPDATE

Ready to rumble, direct loadable robots are now available!  The code is now compatible with Robocode 1.9.0.0 Beta 2.  Download the [code zip here](https://bitbucket.org/singli/rockem-sockem-robocode/downloads/coderobo1_20140116.zip).
